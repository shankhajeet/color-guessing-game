##The Color Guessing Game

*The Project is hosted at gitlab pages. [Click here](https://shankhajeet.gitlab.io/color-guessing-game ) to open url on your native browser. Enjoy*

#Overview
This is a project I created during taking an *Udemy course* of *Colt Steele* called *[The Web Developer Bootcamp](https://www.udemy.com/course/the-web-developer-bootcamp/)*.
I have used *Boostatrp* and *jQuery* to make some of the codes a bit simplified and I also added some of the small yet effective changes in css designs like adding Linear Gradient for the Header and so on to give it my own touch.
The Game is completely Responsive for all the screen resolutions
  
Desktop View :
![](screenShots/Desktop/initial_screen.png)

Mobile View :
![](screenShots/Mobile/initial_screen.png)

#How it works

It is a game of finding the correct color according to its coresponding RGB value,that is displayed on the screen.

Every time we reset, choose to get new colors or change the difficulty,the game randomly chooses auto-generated RGB values from an array and it passes them to the squares. And from that array an RGB value is displayed to the screen for the guessing purpose.

If the player makes a wrong choice the game notifies them that they should try again and makes the block invinsible.

![](screenShots/Desktop/wrong_guess.png)

When the right choice is made all the squares along with the header of the game are inheriting the correct answer's color and a message correct is displayed.

![](screenShots/Desktop/correct_guess.png)

The player can choose between 6 blocks(initial hard difficulty) and 3 blocks(easy).