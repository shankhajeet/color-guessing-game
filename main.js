// colors Arrays
var numSquares = 6;
var colors = generateColors(numSquares);
var squares = document.querySelectorAll('.square');
var pickedColor = pickColor();
var colorDisplay = document.getElementById('colorDisplay');
var messageDisplay = document.getElementById('message');
var resetButton = document.getElementById('playAgain');
var easyButton = document.getElementById('easy');
var hardButton = document.getElementById('hard');

colorDisplay.textContent = pickedColor.toUpperCase();

// reset button
resetButton.addEventListener('click', function() {
    // change the header color
    document.querySelector('.header').style.backgroundColor= '#681cd9';

    // reset the message
    messageDisplay.innerHTML = '';

    // generate new color
    numSquares = 6;
    colors = generateColors(numSquares);

    // pick a new random color
    pickedColor = pickColor();
    colorDisplay.textContent = pickedColor.toUpperCase();

    // Set the square colors
    for(var i = 0; i < squares.length; i++) {
        // add initial colors
        squares[i].style.backgroundColor = colors[i];
    }
})

// easy mode
easyButton.addEventListener('click', function() {
    // change the header color
    document.querySelector('.header').style.backgroundColor= '#681cd9';

    // reset the message
    messageDisplay.innerHTML = '';

    // toggle the active class
    easyButton.classList.add('active');
    hardButton.classList.remove('active');

    // generate new color
    numSquares = 3;
    colors = generateColors(numSquares);

    // pick a new random color
    pickedColor = pickColor();
    colorDisplay.textContent = pickedColor.toUpperCase();

    // Set the square colors
    for(var i = 0; i < squares.length; i++) {
        // filter only top three
        if(colors[i]) {
            // add initial colors
            squares[i].style.backgroundColor = colors[i];
        } else {
            // hide the extras
            squares[i].style.display = 'none';
        }
    }
})

// hard mode
hardButton.addEventListener('click', function() {
    // change the header color
    document.querySelector('.header').style.backgroundColor= '#681cd9';

    // reset the message
    messageDisplay.innerHTML = '';

    // toggle the active class
    hardButton.classList.add('active');
    easyButton.classList.remove('active');

    // generate new color
    numSquares = 6;
    colors = generateColors(numSquares);

    // pick a new random color
    pickedColor = pickColor();
    colorDisplay.textContent = pickedColor.toUpperCase();

    // Set the square colors
    for(var i = 0; i < squares.length; i++) {
        // add initial colors
        squares[i].style.backgroundColor = colors[i];
        squares[i].style.display = 'block';
    }
})

// Loop the color boxes and assign the colors
for(var i = 0; i < squares.length; i++) {
    // add initial colors
    squares[i].style.backgroundColor = colors[i];
    // add click listener
    squares[i].addEventListener('click', function() {
        // grab the clicked color
        var clickedColor = this.style.backgroundColor;
        if(clickedColor === pickedColor) {
            messageDisplay.classList.add("correct");
            messageDisplay.innerHTML = '<i class="fas fa-check-circle mr-2"></i>Correct';
            changeColor(clickedColor);
            resetButton.innerHTML= '<i class="fas fa-redo mr-3"></i>Play Again';
        } else {
            this.style.backgroundColor = "#232323";
            messageDisplay.classList.remove("correct");
            messageDisplay.innerHTML = 'Try Again!'
        }
    })
}
 
// generate colors 
function generateColors (num) {
    // make an array
    var arr = [];

    // add random colors to the array
    for(var i = 0 ; i < num; i++) {
        // get random color and push
        arr.push(randomColor());
    }

    // return the array
    return arr;
}

// Create Colors
function randomColor () {
    // Pick Red from 0 to 255
    var red = Math.floor(Math.random() * 256);
    // Pick Green from 0 to 255
    var green = Math.floor(Math.random() * 256);
    // Pick Blue from 0 to 255
    var blue = Math.floor(Math.random() * 256);
    var rgb = `rgb(${red}, ${green}, ${blue})`

    return rgb;
}

// change Colors
function changeColor (color) {
    // change the header color
    document.querySelector('.header').style.backgroundColor= color;
    // loop through all the squares
    for(var i = 0; i < squares.length; i++) {
        squares[i].style.backgroundColor = color;
    }
}

// pick color function
function pickColor() {
    var random = Math.floor(Math.random() * colors.length);
    return colors[random];
}